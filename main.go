package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type reqData struct {
	ID      int    `json:"id"`
	Date    string `json:"date"`
	DateGmt string `json:"date_gmt"`
	GUID    struct {
		Rendered string `json:"rendered"`
	} `json:"guid"`
	Modified    string `json:"modified"`
	ModifiedGmt string `json:"modified_gmt"`
	Slug        string `json:"slug"`
	Status      string `json:"status"`
	Type        string `json:"type"`
	Link        string `json:"link"`
	Title       struct {
		Rendered string `json:"rendered"`
	} `json:"title"`
	Content struct {
		Rendered  string `json:"rendered"`
		Protected bool   `json:"protected"`
	} `json:"content"`
	Excerpt struct {
		Rendered  string `json:"rendered"`
		Protected bool   `json:"protected"`
	} `json:"excerpt"`
	Author        int           `json:"author"`
	FeaturedMedia int           `json:"featured_media"`
	CommentStatus string        `json:"comment_status"`
	PingStatus    string        `json:"ping_status"`
	Sticky        bool          `json:"sticky"`
	Template      string        `json:"template"`
	Format        string        `json:"format"`
	Meta          []interface{} `json:"meta"`
	Categories    []int         `json:"categories"`
	Tags          []interface{} `json:"tags"`
	PostFormat    []int         `json:"post_format"`
	Links         struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
		About []struct {
			Href string `json:"href"`
		} `json:"about"`
		Author []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"author"`
		Replies []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"replies"`
		VersionHistory []struct {
			Count int    `json:"count"`
			Href  string `json:"href"`
		} `json:"version-history"`
		PredecessorVersion []struct {
			ID   int    `json:"id"`
			Href string `json:"href"`
		} `json:"predecessor-version"`
		WpFeaturedmedia []struct {
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"wp:featuredmedia"`
		WpAttachment []struct {
			Href string `json:"href"`
		} `json:"wp:attachment"`
		WpTerm []struct {
			Taxonomy   string `json:"taxonomy"`
			Embeddable bool   `json:"embeddable"`
			Href       string `json:"href"`
		} `json:"wp:term"`
		Curies []struct {
			Name      string `json:"name"`
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"curies"`
	} `json:"_links"`
	Embedded struct {
		Author []struct {
			ID          int    `json:"id"`
			Name        string `json:"name"`
			URL         string `json:"url"`
			Description string `json:"description"`
			Link        string `json:"link"`
			Slug        string `json:"slug"`
			AvatarUrls  struct {
				Num24 string `json:"24"`
				Num48 string `json:"48"`
				Num96 string `json:"96"`
			} `json:"avatar_urls"`
			Links struct {
				Self []struct {
					Href string `json:"href"`
				} `json:"self"`
				Collection []struct {
					Href string `json:"href"`
				} `json:"collection"`
			} `json:"_links"`
		} `json:"author"`
		WpFeaturedmedia []struct {
			ID    int    `json:"id"`
			Date  string `json:"date"`
			Slug  string `json:"slug"`
			Type  string `json:"type"`
			Link  string `json:"link"`
			Title struct {
				Rendered string `json:"rendered"`
			} `json:"title"`
			Author  int `json:"author"`
			Caption struct {
				Rendered string `json:"rendered"`
			} `json:"caption"`
			AltText      string `json:"alt_text"`
			MediaType    string `json:"media_type"`
			MimeType     string `json:"mime_type"`
			MediaDetails struct {
				Width  int    `json:"width"`
				Height int    `json:"height"`
				File   string `json:"file"`
				Sizes  struct {
					Thumbnail struct {
						File      string `json:"file"`
						Width     int    `json:"width"`
						Height    int    `json:"height"`
						MimeType  string `json:"mime_type"`
						SourceURL string `json:"source_url"`
					} `json:"thumbnail"`
					Medium struct {
						File      string `json:"file"`
						Width     int    `json:"width"`
						Height    int    `json:"height"`
						MimeType  string `json:"mime_type"`
						SourceURL string `json:"source_url"`
					} `json:"medium"`
					MediumLarge struct {
						File      string `json:"file"`
						Width     int    `json:"width"`
						Height    int    `json:"height"`
						MimeType  string `json:"mime_type"`
						SourceURL string `json:"source_url"`
					} `json:"medium_large"`
					Large struct {
						File      string `json:"file"`
						Width     int    `json:"width"`
						Height    int    `json:"height"`
						MimeType  string `json:"mime_type"`
						SourceURL string `json:"source_url"`
					} `json:"large"`
					Full struct {
						File      string `json:"file"`
						Width     int    `json:"width"`
						Height    int    `json:"height"`
						MimeType  string `json:"mime_type"`
						SourceURL string `json:"source_url"`
					} `json:"full"`
				} `json:"sizes"`
				ImageMeta struct {
					Aperture         string        `json:"aperture"`
					Credit           string        `json:"credit"`
					Camera           string        `json:"camera"`
					Caption          string        `json:"caption"`
					CreatedTimestamp string        `json:"created_timestamp"`
					Copyright        string        `json:"copyright"`
					FocalLength      string        `json:"focal_length"`
					Iso              string        `json:"iso"`
					ShutterSpeed     string        `json:"shutter_speed"`
					Title            string        `json:"title"`
					Orientation      string        `json:"orientation"`
					Keywords         []interface{} `json:"keywords"`
				} `json:"image_meta"`
			} `json:"media_details"`
			SourceURL string `json:"source_url"`
			Links     struct {
				Self []struct {
					Href string `json:"href"`
				} `json:"self"`
				Collection []struct {
					Href string `json:"href"`
				} `json:"collection"`
				About []struct {
					Href string `json:"href"`
				} `json:"about"`
				Author []struct {
					Embeddable bool   `json:"embeddable"`
					Href       string `json:"href"`
				} `json:"author"`
			} `json:"_links"`
		} `json:"wp:featuredmedia"`
		WpTerm [][]struct {
			ID       int    `json:"id"`
			Link     string `json:"link"`
			Name     string `json:"name"`
			Slug     string `json:"slug"`
			Taxonomy string `json:"taxonomy"`
			Links    struct {
				Self []struct {
					Href string `json:"href"`
				} `json:"self"`
				Collection []struct {
					Href string `json:"href"`
				} `json:"collection"`
				About []struct {
					Href string `json:"href"`
				} `json:"about"`
				WpPostType []struct {
					Href string `json:"href"`
				} `json:"wp:post_type"`
				Curies []struct {
					Name      string `json:"name"`
					Href      string `json:"href"`
					Templated bool   `json:"templated"`
				} `json:"curies"`
			} `json:"_links"`
		} `json:"wp:term"`
	} `json:"_embedded"`
}

type respData struct {
	ID         int       `json:"id"`
	Title      string    `json:"title"`
	Content    string    `json:"content"`
	CreatedOn  time.Time `json:"created_on"`
	ModifiedOn time.Time `json:"modified_on"`
	Type       string    `json:"type"`
}

func main() {
	url, ok := os.LookupEnv("URL")
	if !ok {
		log.Fatal("URL env var not set")
	}

	var articles []respData
	page := 1

	for {
		resp, hasNext := fetch(url, strconv.Itoa(page))

		for _, v := range resp {
			r := respData{
				ID:         v.ID,
				Title:      v.Title.Rendered,
				CreatedOn:  parseTime(v.Date),
				ModifiedOn: parseTime(v.Modified),
			}
			r.Content, r.Type = fetchContentAndType(v)
			articles = append(articles, r)
		}

		if !hasNext {
			break
		}

		page++
	}

	bts, err := json.Marshal(&articles)
	checkErr(err)

	checkErr(ioutil.WriteFile("data.json", bts, 0644))
}

func fetch(url, page string) ([]reqData, bool) {
	resp, err := http.Get(fmt.Sprintf("%s/wp-json/wp/v2/posts?per_page=100&_embed&page=%s", url, page))
	checkErr(err)

	if resp.StatusCode != 200 {
		log.Fatalf("Expected 200, got: %v", resp.StatusCode)
	}
	defer resp.Body.Close()
	bts, err := ioutil.ReadAll(resp.Body)
	checkErr(err)

	var articles []reqData
	checkErr(json.Unmarshal(bts, &articles))

	return articles, len(articles) == 100
}

func parseTime(strTime string) time.Time {
	tm, err := time.Parse("2006-01-02T15:04:05", strTime)
	checkErr(err)
	return tm
}

func fetchContentAndType(r reqData) (string, string) {
	if r.FeaturedMedia == -1 || len(r.Embedded.WpFeaturedmedia) < 1 {
		return r.Content.Rendered, "text"
	}
	return r.Embedded.WpFeaturedmedia[0].SourceURL, "image"
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
